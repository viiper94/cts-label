<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('status')->default(0);
            $table->string('name', 191);
            $table->string('email', 191);
            $table->date('birth_date');
            $table->string('dj_name', 191)->nullable();
            $table->string('vk', 191)->nullable();
            $table->string('facebook', 191)->nullable();
            $table->string('soundcloud', 191)->nullable();
            $table->string('other_social', 191)->nullable();
            $table->string('phone_number', 191);
            $table->text('address');
            $table->text('education');
            $table->text('job');
            $table->text('sound_engineer_skills')->nullable();
            $table->text('sound_producer_skills')->nullable();
            $table->text('dj_skills')->nullable();
            $table->text('music_genres')->nullable();
            $table->text('additional_info')->nullable();
            $table->text('learned_about_ctschool');
            $table->text('what_to_learn')->nullable();
            $table->text('purpose_of_learning')->nullable();
            $table->timestamp('closed_at')->nullable();
            $table->timestamps();
            $table->string('course', 191);
            $table->text('os')->nullable();
            $table->text('equipment')->nullable();
            $table->string('document', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv');
    }
}
