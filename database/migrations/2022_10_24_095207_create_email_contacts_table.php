<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('full_name', 191)->nullable();
            $table->string('email', 191);
            $table->text('company')->nullable();
            $table->text('position')->nullable();
            $table->text('additional')->nullable();
            $table->string('lang', 191)->nullable()->default('en');
            $table->text('email_channels')->nullable();
            $table->timestamps();
            $table->string('phone', 191)->nullable();
            $table->string('website', 191)->nullable();
            $table->string('country', 191)->nullable();
            $table->string('company_foa', 191)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_contacts');
    }
}
