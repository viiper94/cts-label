<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_queue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('channel_id');
            $table->text('subject');
            $table->string('from', 191);
            $table->string('to', 191);
            $table->string('name', 191);
            $table->boolean('sent')->default(false);
            $table->timestamps();
            $table->integer('sort')->default(0);
            $table->integer('error_code')->nullable();
            $table->text('error_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_queue');
    }
}
