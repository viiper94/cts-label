<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sort_id')->nullable();
            $table->integer('release_id')->nullable();
            $table->string('feedback_title', 191);
            $table->string('archive_name', 191)->nullable();
            $table->text('tracks');
            $table->boolean('visible')->default(true);
            $table->timestamps();
            $table->string('slug', 191)->nullable();
            $table->string('image', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
