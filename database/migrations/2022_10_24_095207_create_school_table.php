<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sort_id')->default(0);
            $table->string('category', 191);
            $table->string('lang', 191);
            $table->text('name')->nullable();
            $table->text('teacher_binfo')->nullable();
            $table->text('teacher_hinfo')->nullable();
            $table->string('image', 191)->nullable();
            $table->text('course_alt')->nullable();
            $table->timestamps();
            $table->boolean('visible')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school');
    }
}
